import React from "react";
import ReactDOM from "react-dom";
import Header from "./components/header_and_footer/Header";
import Footer from "./components/header_and_footer/Footer";
import AutosuggestSearchApp from "./components/app/AutosuggestSearchApp";
import "./index.css";

ReactDOM.render(
  <>
    {/* <React.StrictMode> */}
    <Header />
    <div id="easMainContent" className="eas-main-content">
      <AutosuggestSearchApp />
    </div>
    <Footer />
    {/* </React.StrictMode> */}
  </>,
  document.getElementById("root")
);
