import React, { Component } from "react";
import Checkbox from '@material-ui/core/Checkbox';
import JobDetailPopUp from "../../util/JobDetailPopUp";

class JobDetail extends Component {
  constructor(props) {
    super(props);
    this.state = { displayPopUp: false, jobSelected: false };
    this.jobDetail = props.jobDetail;
    this.index = props.index;
    this.updateSelectedJobs = props.updateSelectedJobs;
  }

  togglePopUp = () => {
    this.setState(() => ({ displayPopUp: !this.state.displayPopUp }));
  };

  updateSelectedJob = event => {
    if (event.target.checked) {
      this.setState(() => ({ jobSelected: true }));
    } else {
      this.setState(() => ({ jobSelected: false }));
    }
    this.updateSelectedJobs(event, this.jobDetail);
  };

  render() {
    this.detailedJobInfo = (
      <table className="eas-app-detail-table">
        <tbody>
          <tr>
            <td className="eas-app-detail-table-title">ID: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.id} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Job-Code: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.jobcode} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Domain: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.domain} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Location: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.location} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Experience: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.experience} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Title: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.title} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Responsibility: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.responsibility} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Primary skill: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.primary_skill} </td>
          </tr>
          <tr>
            <td className="eas-app-detail-table-title">Secondary skill: </td>
            <td className="eas-app-detail-table-value">{this.jobDetail.secondary_skill} </td>
          </tr>
        </tbody>
      </table>
    );

    return (
      <div
        className={
          "eas-app-job-content eas-inline-block" +
          (this.state.jobSelected ? " eas-selected-job" : "")
        }
      >
        <div className="eas-inline-block">
          <Checkbox
            color="default"
            id={"job_chk_box_" + this.index}
            name={"job_chk_box_" + this.index}
            onClick={e => this.updateSelectedJob(e)}
          />
        </div>
        <div className="eas-inline-block" onClick={this.togglePopUp}>
        {this.jobDetail.id + " "}{this.jobDetail.title}
        </div>
        {this.state.displayPopUp ? (
          <JobDetailPopUp
            title="Job Details"
            content={this.detailedJobInfo}
            toggle={this.togglePopUp}
          />
        ) : null}
      </div>
    );
  }
}

export default JobDetail;
