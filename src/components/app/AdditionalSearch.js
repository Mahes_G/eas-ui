import React, { Component } from "react";
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import JobDetail from "../jobDetail/JobDetail";
import "./AdditionalSearch.css";
import SubmitSelJobsPopUp from "../../util/SubmitSelJobsPopUp";

class AdditionalSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totalJobsSelected: 0,
      displaySubmitPopUp: false,
      jobsListToBeUpdated: false
    };
    this.jobsData = props.jobDetails;
    this.titleSearchVal = props.titleSearchVal;
    this.locSearchVal = props.locSearchVal;
    this.uniqueJobLocations = props.uniqueJobLocations;
    this.additionalLocations = "";
    this.includeAnyLocation = false;
    this.consolidatedLocations = new Set();
    this.selectedJobs = {};
    this.selListOfJobs = [];;
    this.jobsListData = [];
    this.totalPages = 0;
    this.jobsPerPage = 4;
    this.currentPage = 1;
    this.jobsListUIContent = [];
  }

  updateAnyLocation = (event) => {
    if (event.target.checked) {
      this.includeAnyLocation = true;
    } else {
      this.includeAnyLocation = false;
    }
  };

  updateSelectedJobs = (event, jobDetail) => {
    if (event.target.checked) {
      this.selectedJobs[jobDetail.id] = jobDetail;
      this.setState(() => ({
        totalJobsSelected: this.state.totalJobsSelected + 1
      }));
    } else {
      this.selectedJobs[jobDetail.id] = null;
      this.setState(() => ({
        totalJobsSelected: this.state.totalJobsSelected - 1
      }));
    }
  };

  handleInput = (event) => {
    this.additionalLocations = event.target.value;
  };

  updatePageNum = () => {
    console.log('In updatePageNum(): this.currentPage: ' + this.currentPage);
    this.updatePageContent(this.currentPage);
  };

  changePageNumberVal = (event) => {
    console.log("changePageNumberVal:");
    var pageVal = event.target.value;
    console.log("pageVal: " + pageVal);
    if (pageVal !== undefined && pageVal > 0 && pageVal <= this.totalPages) {
      this.currentPage = pageVal;
    }
  };

  updatePageContent = (pageNum) => {
    this.jobsListUIContent = [];

    if (this.totalPages > 0) {
      var currPageStartIndex = this.jobsPerPage * (pageNum - 1);
      var currPageEndIndex = currPageStartIndex + this.jobsPerPage;
      console.log("currPageStartIndex: " + currPageStartIndex);
      console.log("currPageEndIndex: " + currPageEndIndex);
      for (
        var i = currPageStartIndex;
        i < currPageEndIndex && i < this.jobsListData.length;
        i++
      ) {
        var currJobDetail = this.jobsListData[i];
        var currJobUI = (
          <JobDetail
            key={i + "_" + currJobDetail.id}
            index={i + 1}
            jobDetail={currJobDetail}
            updateSelectedJobs={this.updateSelectedJobs}
          />
        );
        this.jobsListUIContent.push(currJobUI);
      }
    }

    this.setState(() => ({jobsListToBeUpdated: true}));
  }

  searchJob = () => {
    let conLocations = new Set();
    if (this.includeAnyLocation === false) {
      conLocations.add(null);
      conLocations.add("");
      conLocations.add(this.locSearchVal.toLowerCase());
      this.additionalLocations
        .split(",")
        .map((location, key) =>
          conLocations.add(location.trim().toLowerCase())
        );
      conLocations.delete("");
      conLocations.delete(null);
    }
    this.consolidatedLocations = conLocations;
    this.jobsListData = this.jobsData
      .filter(
        (j) =>
          (j.title === undefined ||
            j.title.length === 0 ||
            j.title
              .toLowerCase()
              .includes(this.titleSearchVal.toLowerCase())) &&
          (this.consolidatedLocations.size === 0 ||
            this.consolidatedLocations.has(j.location.toLowerCase()))
      );
    this.totalPages = Math.ceil(this.jobsListData.length / this.jobsPerPage);
    this.updatePageContent(1);
  };

  togglePopUp = () => {
    this.setState(() => ({
      displaySubmitPopUp: !this.state.displaySubmitPopUp
    }));
  };

  submitSelJob() {
    this.togglePopUp();
    this.selListOfJobs = [];
    Object.keys(this.selectedJobs).forEach((key) => {
      let val = this.selectedJobs[key];
      if (val !== null) {
        this.selListOfJobs.push(val);
      }
    });
  }

  render() {
    return (
      <div>
        <fieldset>
          <legend>Willing to relocate ?</legend>
          <div className="eas-app-block">
            <Checkbox
              id="withinState"
              name="withinState"
              value="withinState"
              disabled
            />
            <label htmlFor="withinState">Within State</label>
          </div>
          <div className="eas-app-block">
            <Checkbox
              color="primary"
              id="anyLocation"
              name="anyLocation"
              value="anyLocation"
              onClick={(e) => this.updateAnyLocation(e)}
            />
            <label htmlFor="anyLocation">Any Location</label>
          </div>
          <div className="eas-app-block">
            <TextField
              id="otherLocations"
              name="otherLocations"
              label="Enter other locations to include seprated by comma(,)"
              variant="outlined"
              className="inp-other-loc"
              onChange={this.handleInput}
            />
          </div>
        </fieldset>
        <div className="ess-app-right-align">
          <Button variant="contained" color="primary" onClick={() => this.searchJob()}>Search</Button>
        </div>
        <div className="ess-app-centre-align">{this.jobsListUIContent}</div>
        {this.totalPages > 0 && (
          <div className="ess-app-right-align">
            <Button variant="contained" size="small" onClick={() => this.updatePageNum()}>
              Goto
            </Button>
            {" "}Page{" "}
            <TextField
              id="pageNumberVal"
              name="pageNumberVal"
              className="inp-page-num"
              onChange={this.changePageNumberVal}
            />{" "} of {this.totalPages}
          </div>
        )}
        {this.state.totalJobsSelected > 0 && (
          <div className="ess-app-centre-align">
            <Button variant="contained" color="primary" size="large" onClick={() => this.submitSelJob()}>
              Apply for selected Jobs
            </Button>
          </div>
        )}
        {this.state.displaySubmitPopUp && (
          <SubmitSelJobsPopUp
            title="Apply for selected Jobs"
            selListOfJobs={this.selListOfJobs}
            toggle={this.togglePopUp}
          />
        )}
      </div>
    );
  }
}

export default AdditionalSearch;
