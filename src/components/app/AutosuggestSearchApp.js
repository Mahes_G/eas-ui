import React from "react";
import Button from '@material-ui/core/Button';
import Autosuggest from "react-autosuggest";
import AdditionalSearch from "./AdditionalSearch";
import "../../assets/Autosuggest.css";
import "./AutosuggestSearchApp.css";
import API from "../../service/API";

let uniqueJobLocations = [];
let jobDetails = [];

function getSuggestionsForTitle(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return jobDetails.filter(jobDetail =>
    jobDetail.title.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionsForLoc(value) {
  const escapedValue = value.trim();

  if (escapedValue === "") {
    return [];
  }

  return uniqueJobLocations.filter(jobLocation =>
    jobLocation.location.toLowerCase().includes(escapedValue.toLowerCase())
  );
}

function getSuggestionValueForTitle(suggestion) {
  return suggestion.title;
}

function getSuggestionValueForLoc(suggestion) {
  return suggestion.location;
}

function renderSuggestionForTitle(suggestion) {
  return <span>{suggestion.title}</span>;
}

function renderSuggestionForLoc(suggestion) {
  return <span>{suggestion.location}</span>;
}

class AutosuggestSearchApp extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      titleSearchVal: "",
      suggestionsForTitle: [],
      locSearchVal: "",
      suggestionsForLoc: [],
      loadedApiData: false,
      searchOrResetButtonMode: 0 // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    };

    this.autoSearchUI = <p>Loading...</p>;
  }

  convertSetToArray = (val1, val2, setItself) => {
    uniqueJobLocations.push({ location: val1 });
  };

  loadUniqueJobLoc = () => {
    // Loading unique locations to avoid displaying duplicate search results
    uniqueJobLocations = [];
    let uniqueSet = new Set();
    jobDetails.map((jobData, key) => uniqueSet.add(jobData.location));
    uniqueSet.forEach(this.convertSetToArray);
  };

  async componentDidMount() {
    let userData;
    try {
      userData = await API.get("/all");
      if (
        userData !== undefined &&
        userData !== null &&
        userData.data !== undefined
      ) {
        jobDetails = userData.data;
        this.loadUniqueJobLoc();
        this.setState(() => ({ loadedApiData: true }));
      }
    } catch (e) {
      console.log(`Axios request failed: ${e}`);
    }
  }

  onChangeForTitle = (event, { newValue, method }) => {
    this.setState({
      titleSearchVal: newValue
    });
  };

  onChangeForLoc = (event, { newValue, method }) => {
    this.setState({
      locSearchVal: newValue
    });
  };

  onSuggestionsFetchRequestedForTitle = ({ value }) => {
    this.setState({
      suggestionsForTitle: getSuggestionsForTitle(value)
    });
  };

  onSuggestionsFetchRequestedForLoc = ({ value }) => {
    this.setState({
      suggestionsForLoc: getSuggestionsForLoc(value)
    });
  };

  onSuggestionsClearRequestedForTitle = () => {
    this.setState({
      suggestionsForTitle: []
    });
  };

  onSuggestionsClearRequestedForLoc = () => {
    this.setState({
      suggestionsForLoc: []
    });
  };

  updateAvailabilityOfAutoSearchInputFields = flag => {
    // The following piece of code is necessary to overide the 3rd party autosearch component
    document
      .getElementById("autoSuggestDivForTitle")
      .getElementsByTagName("input")[0].disabled = flag;
    document
      .getElementById("autoSuggestDivForLoc")
      .getElementsByTagName("input")[0].disabled = flag;
  };

  showDetail = () => {
    // 'searchOrResetButtonMode' is 0 for "Search" and 1 for "Reset"
    if (this.state.searchOrResetButtonMode === 0) {
      this.updateAvailabilityOfAutoSearchInputFields(true);
      this.additionalSearchComp = (
        <AdditionalSearch
          titleSearchVal={this.state.titleSearchVal}
          locSearchVal={this.state.locSearchVal}
          uniqueJobLocations={uniqueJobLocations}
          jobDetails={jobDetails}
        />
      );
    } else {
      this.updateAvailabilityOfAutoSearchInputFields(false);
      this.additionalSearchComp = <></>;
    }
    this.setState({
      searchOrResetButtonMode: this.state.searchOrResetButtonMode === 0 ? 1 : 0
    });
  };

  render() {
    const {
      titleSearchVal,
      suggestionsForTitle,
      locSearchVal,
      suggestionsForLoc
    } = this.state;

    const inputPropsForTitle = {
      placeholder: "Enter Job Title (Partial Match)",
      value: titleSearchVal,
      onChange: this.onChangeForTitle
    };

    const inputPropsForLoc = {
      placeholder: "Enter Job Location (Exact Match)",
      value: locSearchVal,
      onChange: this.onChangeForLoc
    };

    if (this.state.loadedApiData) {
      this.autoSearchUI = (
        <>
          <div id="autoSuggestDivForTitle" className="eas-app-block">
            <Autosuggest
              suggestions={suggestionsForTitle}
              onSuggestionsFetchRequested={
                this.onSuggestionsFetchRequestedForTitle
              }
              onSuggestionsClearRequested={
                this.onSuggestionsClearRequestedForTitle
              }
              getSuggestionValue={getSuggestionValueForTitle}
              renderSuggestion={renderSuggestionForTitle}
              inputProps={inputPropsForTitle}
              id="auto-suggest-title"
            />
          </div>
          <div id="autoSuggestDivForLoc" className="eas-app-block">
            <Autosuggest
              suggestions={suggestionsForLoc}
              onSuggestionsFetchRequested={
                this.onSuggestionsFetchRequestedForLoc
              }
              onSuggestionsClearRequested={
                this.onSuggestionsClearRequestedForLoc
              }
              getSuggestionValue={getSuggestionValueForLoc}
              renderSuggestion={renderSuggestionForLoc}
              inputProps={inputPropsForLoc}
              id="auto-suggest-location"
            />
          </div>
          <div className="eas-app-block">
            <Button variant="contained" color={this.state.searchOrResetButtonMode === 0 ? "primary" : "secondary"} size="large" onClick={() => this.showDetail()}>
              {this.state.searchOrResetButtonMode === 0 ? "Search" : "Reset"}
            </Button>
          </div>
        </>
      );
    }

    return (
      <div>
        {this.autoSearchUI}
        {this.additionalSearchComp}
      </div>
    );
  }
}

export default AutosuggestSearchApp;
