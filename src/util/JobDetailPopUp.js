import React, { Component } from "react";

export default class JobDetailPopUp extends Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  handleClick = () => {
    this.props.toggle();
  };

  render() {
    return (
      <div className="modal">
        <div className="modal_content">
          <span className="modal-popup-title">
            {this.props.title}
          </span>
          <span className="modal-popup-close" onClick={this.handleClick}>
            &times;
          </span>
          {this.props.content}
        </div>
      </div>
    );
  }
}
